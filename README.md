# Snibox dmenu

This script does print all my snippets (from my Snippet API project) to dmenu.

## Preview
![image](/demo.gif)

## How to setup
1. Clone the repo
2. Edit the script ``download_from_snibox.sh`` for your API Host
3. Run the script to download the latest snippets
4. Edit the script ``snibox_dmenu.sh`` with your script folder path (for execute)
5. Run the script and now you should see dmenu
