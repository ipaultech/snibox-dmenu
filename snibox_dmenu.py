import subprocess
import sys
import json
import os

#===============
#Output specific

#Dmenu search Prompt
dmenu_search_prompt = "Snippets:" 
#Dmenu search Prompt for multiple files in one snippet (2. entry)
dmenu_filesearch_prompt = "File:"

#Dmenu arguments
#Note: "-i" is for ignore case sensitive
#Horizontal
dmenu_args = ["-i"]

#Vertical (if you want that ;) )
#dmenu_args = ["-l", "10", "-i"]

#================

#DMENU function
def dmenu(options=None, prompt=None ,dmenu_options=None):
    """
    Opens dmenu with given options and returns the index of the chosen option
    Arguments
    ------------
    options: list of strings to be selected from
    prompt: info text in front of selection
    dmenu_options: list of options to be passed to dmenu (see man dmenu)
    Returns
    ------------
    index of selected item (None in case of failure e.g. due to no selection)
    """
    command = ["dmenu"]
    if prompt != None:
        command += ["-p", f"{prompt}"]
    if dmenu_options != None:
        command += dmenu_options if type(dmenu_options) == list else [dmenu_options]
    proc = subprocess.Popen(
        command, stdin=subprocess.PIPE, stdout=subprocess.PIPE
    )

    for option in options:
        proc.stdin.write((option + "\n").encode())
    stdout, _ = proc.communicate()

    try:
        index = options.index(stdout.splitlines()[0].decode())
    except Exception:
        return

    return index

#============

#JSON File Handling

# check arguments passed to script
if len(sys.argv) < 2:
    print("usage: python3", sys.argv[0], "[JSON file]" ,file=sys.stderr)
    exit()
else:
    file = open(sys.argv[1],"r")
    file_dir = os.path.dirname(os.path.realpath(sys.argv[1]))



#Load data into JSON
#data = json.loads(site_string)
data = json.load(file)

#Define arrays
# Display text for DMENU
options = []
# Array for content IDs
sniboxid = []

#For every snippet from JSON do...
for id in data:

    #Get Snippet ID and parse it to array
    str_id = id['id']
    sniboxid.append(str_id)

    #Get Label Name
    str_label_name = id['label_name']
    
    if str_label_name == "":
        #If Empty Lable then format Output to nothing
        strc_label_name = ""
    else:
        #If a Label exist add a "-" to it for seperation
        strc_label_name = str_label_name + "-"

    #Snippet title name
    strc_title_name = id['title']

    #Finished option string for dmenu array
    strc_option = strc_label_name + strc_title_name
    #Add to array
    options.append(strc_option)

#Call DMENU for entry

#DMENU Options
#Prompt message
prompt = dmenu_search_prompt
#Dmenu arguments
dmenu_options = dmenu_args

#Call dmenu function
index = dmenu(options,prompt,dmenu_options)
if index == None:
    print("[No option selected]")
    exit()

#======
#PROMPT ANSWER PROCESSING

#Initalize Array for Snippets
strc_snippet_title_array = []
strc_snippet_content_array = []

#For every snippet in json do...
for id in data:
    #If snippet ID from DMENU answer is found do...
    if id['id'] == sniboxid[index]:
        #For every snippet file do...
        for id in id['snippetFiles']:
            #Add title to title array
            strc_snippet_title_array.append(id['title'])
            #Add content to content array
            strc_snippet_content_array.append(id['content'])

#If more than one snippet file exist ask for specific file...
if len(strc_snippet_title_array) > 1:
    #Another DMENU call
    #Change prompt
    prompt = dmenu_filesearch_prompt
    index = dmenu(strc_snippet_title_array,prompt,dmenu_options)
    if index == None:
        print("[No option selected]")
        exit()

    #Print selected content
    print(strc_snippet_content_array[index])

#If only one snippet file exist, print it!
else:
    print(strc_snippet_content_array[0])



file.close()



#=======
#REFERENCES
#=======

#Much thanks to Denis Gessert
#for the dmenu json examples
#https://gitlab.com/denges/dmenujson
